//
//  GOL.h
//  09001997 FYP
//
//  Created by BarryMcNamara on 28/03/2013.
//
//  This implementation of John Conway's Game of Life CA is based on
//  code from Daniel Shiffman's Nature of Code book. The original
//  version was written in the Processing language and can be found at
//  https://github.com/shiffman/The-Nature-of-Code


#pragma once
#include <iostream>
#include "vector.h"
#include "ofMain.h"

class GOL {
    
public:
    int w = 24;
    int rows = 32;
    int cols = 32;
    int interval = -5;
    bool active;
    
    vector <int> board;
    vector<int> next;
    vector<float> fltBoard;
    
    ofColor col;
    
    GOL();
    void init();
    void generate();
    void display();
    void update();
    void convertVec();
    void pause();
    void resume();
};