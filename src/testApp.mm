
// This class creates an instance of openFrameworks testApp which is called by ofRunApp() in main.mm

// UI functionality was built using ofxUI, an openFrameworks addon by Reza Ali.
// https://github.com/rezaali/ofxUI
// Communication between openFrameworks and Pure Data was achieved using ofxPd, an openFrameworks addon by Dan Wilcox.
// https://github.com/danomatika/ofxPd


#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup() {
    
    // Initialize openFrameworks parameters, background color, framerate etc.
	iPhoneSetOrientation(OFXIPHONE_ORIENTATION_LANDSCAPE_RIGHT);
	ofBackground(0);
    ofSetVerticalSync(false);
    ofEnableSmoothing();
    ofSetFrameRate(60);
    
    // Initialize ofxPd / Pure Data communication
    int ticksPerBuffer = 8;
	pd.init(2, 0, 44100, ticksPerBuffer);
    pd.openPatch("arrayTest2.pd");
    pd.start();
    ofSoundStreamSetup(2, 0, this, 44100, ofxPd::blockSize() * ticksPerBuffer, 1);
    
    // Draw user interface elements, sets up initial values and ranges, names elements so that
    // values can be received in Pure Data patch
    gui = new ofxUICanvas(768,0,256,768);
    gui -> addSpacer();
    gui -> addWidgetDown(new ofxUILabel("Automata Control", OFX_UI_FONT_LARGE));
    gui -> addSpacer();
    gui -> addWidgetDown(new ofxUILabel("Update Rate", OFX_UI_FONT_MEDIUM));
    gui -> addSpacer();
    gui -> addWidgetDown(new ofxUISlider(246, 44, -20.0, -1.0, -5.0, "Update Rate"));
    gui -> addToggle("Pause Automata", false, 48, 48);
    gui -> addSpacer();
    gui -> addWidgetDown(new ofxUILabel("Synth Control", OFX_UI_FONT_LARGE));
    gui -> addSpacer();
    gui -> addWidgetDown(new ofxUILabel("Carrier Frequency", OFX_UI_FONT_MEDIUM));
    gui -> addWidgetDown(new ofxUISlider(248, 44, 50.0, 1000.0, 250.0, "CF"));
    gui -> addWidgetDown(new ofxUILabel("Modulation Frequency", OFX_UI_FONT_MEDIUM));
    gui -> addWidgetDown(new ofxUISlider(248, 44, 20.0, 1000.0, 150.0, "MF"));
    gui -> addWidgetDown(new ofxUILabel("Modulation Amount", OFX_UI_FONT_MEDIUM));
    gui -> addWidgetDown(new ofxUISlider(248, 44, 1.0, 100.0, 50.0, "MA"));
    gui -> addWidgetDown(new ofxUILabel("Low Pass Filter", OFX_UI_FONT_MEDIUM));
    gui -> addWidgetDown(new ofxUISlider(248, 44, 0.0, 15000.0, 5000.0, "Low Pass"));
    gui -> setWidgetColor(OFX_UI_WIDGET_COLOR_BACK, ofColor(0, 120, 120));
    ofAddListener(gui -> newGUIEvent, this, &testApp::guiEvent);
    gui -> loadSettings("GUI/guiSettings.xml");
    
    // Set colors of user interface elements
    gui -> setWidgetColor(OFX_UI_WIDGET_COLOR_OUTLINE, ofColor(0, 255, 255));
    gui -> setWidgetColor(OFX_UI_WIDGET_COLOR_OUTLINE_HIGHLIGHT, ofColor(0, 255, 255));
    gui -> setWidgetColor(OFX_UI_WIDGET_COLOR_FILL, ofColor(0, 255, 255));
    gui -> setWidgetColor(OFX_UI_WIDGET_COLOR_FILL_HIGHLIGHT, ofColor(0, 255, 255));
    
    // Initialize Game of Life
    gol.init();
}



//--------------------------------------------------------------
// This function is called at each frame of the application. Sends a 'bang' to update the array in Pure Data.
// Writes the array of floats created by gol.convertVec() to the array in Pure Data

void testApp::update() {
    gol.convertVec();
    pd.sendBang("bangTest");
    for (int i = 0; i < gol.fltBoard.size(); i ++) {
        pd.writeArray("array1", gol.fltBoard);
    }
}



//--------------------------------------------------------------
// Calls the display and update functionality from the GOL class, draws auomata to screen at each frame

void testApp::draw() {
    gol.update();
    gol.display();
}



//--------------------------------------------------------------
// Establishes audio output for the application via ofxPd and Pure Data

void testApp::audioOut(float *output, int bufferSize, int numChannels) {
	pd.audioOut(output, bufferSize, numChannels);
}



//--------------------------------------------------------------
// GUI call back function, passes values from GUI to Pure Data via pd.sendFloat(), implements Upate Rate and Pause functionality

void testApp::guiEvent(ofxUIEventArgs &e) {
    if(e.widget->getName() == "Update Rate") {
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        gol.interval = slider -> getScaledValue();
    }
    else if(e.widget->getName() == "Pause Automata") {
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        if (toggle -> getValue() == 1) {
            gol.pause();
        } else {
            gol.resume();
        }
    }
    else if (e.widget -> getName() == "CF") {
        ofxUISlider *slider2 = (ofxUISlider *) e.widget;
        pd.sendFloat("carrierFreq", slider2 -> getScaledValue());
    }
    else if (e.widget -> getName() == "MF") {
        ofxUISlider *slider3 = (ofxUISlider *) e.widget;
        pd.sendFloat("modDepth", slider3 -> getScaledValue());
    }
    else if (e.widget -> getName() == "MA") {
        ofxUISlider *slider4 = (ofxUISlider *) e.widget;
        pd.sendFloat("modAmount", slider4 -> getScaledValue());
    }
    else if (e.widget -> getName() == "Low Pass") {
        ofxUISlider *slider5 = (ofxUISlider *) e.widget;
        pd.sendFloat("lowPass", slider5 -> getScaledValue());
    }
}



//--------------------------------------------------------------
// Deletes GUI when application is closed, stores UI values as XML data

void testApp::exit(){
    gui -> saveSettings("GUI/guiSettings.xml");
    delete gui;
}



//--------------------------------------------------------------
void testApp::touchDown(ofTouchEventArgs & touch){
}



//--------------------------------------------------------------
void testApp::touchMoved(ofTouchEventArgs & touch){
    
}



//--------------------------------------------------------------
void testApp::touchUp(ofTouchEventArgs & touch){
    
}



//--------------------------------------------------------------
// Enables restart of the automata via double tap on screen, implements a bounds check
// to ensure there are no accidental restarts when using UI

void testApp::touchDoubleTap(ofTouchEventArgs & touch){
    if (touch.x <= ofGetWidth() - 256) {
        gol.init();
        gol.display();
    }
    
}

//--------------------------------------------------------------
void testApp::touchCancelled(ofTouchEventArgs & touch){
    
}

//--------------------------------------------------------------
void testApp::lostFocus(){
    
}

//--------------------------------------------------------------
void testApp::gotFocus(){
    
}

//--------------------------------------------------------------
void testApp::gotMemoryWarning(){
    
}

//--------------------------------------------------------------
void testApp::deviceOrientationChanged(int newOrientation){
    
}

