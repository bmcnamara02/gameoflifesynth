//
//  GOL.cpp
//  09001997 FYP
//
//  Created by BarryMcNamara on 28/03/2013.
//
//  This implementation of John Conway's Game of Life CA is based on
//  code from Daniel Shiffman's Nature of Code book (2012). The original
//  version was written in the Processing language and can be found at
//  https://github.com/shiffman/The-Nature-of-Code

#include "GOL.h"

// Constructor
GOL::GOL() {
}



//--------------------------------------------------------------
// Initializes the CA grid using a random generator, is called when the app starts and when
// void testApp::touchDoubleTap is called in testApp.mm

void GOL::init() {
    for (int i =0; i < cols; i ++) {
        for (int j =0; j < rows; j ++) {
            board.push_back(rows * cols);
            board[i * cols + j] = ofRandom(2);
        }
    }
}



//--------------------------------------------------------------
// Is called at each frame in testApp.mm, the modulo operator allows for control over the update
// rate of the automata independent of the framerate of the application

void GOL::update() {
    if (ofGetFrameNum() % interval == 0 && active) {
        generate();
    }
}



//--------------------------------------------------------------
// Creates a new generation of the automata each time GOL::update is called in testApp.mm
// Loops through the board vector and checks the neighbours of each point in the grid
// Applies the rules of Conway's Game of Life to each cell and updates the vector next as the current generation

void GOL::generate() {
    vector<int> next(rows * cols);
    for (int x = 0; x < cols; x++) {
        for (int y = 0; y < rows; y++) {
            int neighbors = 0;
            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    neighbors += board[((x + i + cols) % cols) * cols + ((y + j + rows) % rows)];
                }
            }
            neighbors -= board[x * cols + y];
            
            // Rules of Life
            if      ((board[x * cols + y] == 1) && (neighbors <  2)) next[x * cols + y] = 0;    // Loneliness
            else if ((board[x * cols + y] == 1) && (neighbors >  3)) next[x * cols + y] = 0;    // Overpopulation
            else if ((board[x * cols + y] == 0) && (neighbors == 3)) next[x * cols + y] = 1;    // Reproduction
            else                               next[x * cols + y] = board[x * cols + y];        // Stasis
        }
    }
    board = next;
}



//--------------------------------------------------------------
// Draws the automata to the screen, is called at each frame of the application in the draw function in testApp.mm

void GOL::display() {
    for ( int i = 0; i < cols;i++) {
        for ( int j = 0; j < rows;j++) {
            if (board[i * cols + j] == 1) {
                col.set(0, 255, 255);
                ofSetColor(col);
            } else {
                col.set(0);
                ofSetColor(col);
            } ofRect(i * w, j * w, w , w);
        }
    }
}



//--------------------------------------------------------------
// Converts the vector of integers representin the automata to a vector of floats to be passed to the Pure Data patch

void GOL::convertVec() {
    fltBoard.resize(board.size());
    std::copy(board.begin(), board.end(), fltBoard.begin());
}



//--------------------------------------------------------------
// Pauses the automata

void GOL::pause() {
    active = false;
}



//--------------------------------------------------------------
// Resumes the automata after it has been paused

void GOL::resume() {
    active = true;
}



