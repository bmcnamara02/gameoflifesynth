#pragma once

#include "ofMain.h"
#include "ofxiPhone.h"
#include "ofxiPhoneExtras.h"
#include "ofxPd.h"
#include "ofxUI.h"
#include "GOL.h"


class testApp : public ofxiPhoneApp {
public:
    void setup();
    void update();
    void draw();
    void exit();
	
    void touchDown(ofTouchEventArgs & touch);
    void touchMoved(ofTouchEventArgs & touch);
    void touchUp(ofTouchEventArgs & touch);
    void touchDoubleTap(ofTouchEventArgs & touch);
    void touchCancelled(ofTouchEventArgs & touch);
    
    void lostFocus();
    void gotFocus();
    void gotMemoryWarning();
    void deviceOrientationChanged(int newOrientation);
    void run();
    
    ofxUICanvas *gui;
    void guiEvent(ofxUIEventArgs &e);
    
    GOL gol;
    
    ofxPd pd;
    void audioOut(float *output, int bufferSizr, int numChannels);
};